import 'dart:convert';
import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'package:shop_app/helpers/server.dart';

class AuthProvider with ChangeNotifier {
  String? _token;
  DateTime? _expiryDate;
  String? _userId;
  Timer? _authTimer;

  bool get isAuthenticated {
    return token != '';
  }

  String get userId {
    if (_userId != null) {
      return _userId!;
    }

    return '';
  }

  String get token {
    if (_expiryDate != null &&
        _expiryDate!.isAfter(DateTime.now()) &&
        _token != null) {
      return _token!;
    }
    return '';
  }

  Future<void> _authenticate(
      String username, String password, String urlSegment) async {
    final String host = Server.host;
    final Uri url = Uri.parse(host + '$urlSegment');
    final data = {
      "username": username,
      "password": password,
    };

    final String basicAuth =
        'Basic ' + base64Encode(utf8.encode('$username:$password'));

    try {
      final response = await http.post(
        url,
        body: json.encode(data),
        headers: {
          HttpHeaders.authorizationHeader: basicAuth,
          HttpHeaders.contentTypeHeader: "application/json",
        },
      );
      final extendedData = json.decode(response.body);
      print(extendedData);

      _token = extendedData['token'];
      _userId = extendedData['userId'].toString();
      _expiryDate = DateTime.now().add(
        Duration(seconds: extendedData['expiration']),
      );

      _autoLogout();
      notifyListeners();

      final prefs = await SharedPreferences.getInstance();
      final Map userData = {
        'token': _token,
        'userId': _userId,
        'expiryDate': _expiryDate!.toIso8601String(),
      };
      final String userDataString = json.encode(userData);
      prefs.setString('userData', userDataString);
    } catch (error) {
      print(error);
      throw error;
    }
  }

  Future<void> signup(String username, String password) async {
    return _authenticate(username, password, 'api/users');
  }

  Future<void> login(String username, String password) async {
    return _authenticate(username, password, 'api/token');
  }

  Future<bool> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }

    final String? prefUserData = prefs.getString('userData');
    final String extractedUserData = prefUserData ?? '';
    final userData = json.decode(extractedUserData) as Map<String, dynamic>;
    final expiryDate = DateTime.parse(userData['expiryDate']);
    if (expiryDate.isBefore(DateTime.now())) {
      return false;
    }

    _token = userData['token'];
    _userId = userData['userId'];
    _expiryDate = expiryDate;

    notifyListeners();
    _autoLogout();

    return true;
  }

  Future<void> logout() async {
    _token = '';
    _userId = '';
    _expiryDate = null;

    if (_authTimer != null) {
      _authTimer!.cancel();
      _authTimer = null;
    }
    notifyListeners();

    final prefs = await SharedPreferences.getInstance();
    // removes only the userData key
    // prefs.remove('userData');

    // removes everythin from stored data
    prefs.clear();
  }

  void _autoLogout() {
    if (_authTimer != null) {
      _authTimer!.cancel();
    }

    final timeToExpiry = _expiryDate!.difference(DateTime.now()).inSeconds;
    _authTimer = Timer(Duration(seconds: timeToExpiry), logout);
  }
}
