import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:shop_app/helpers/server.dart';

class Product with ChangeNotifier {
  final String id;
  final String title;
  final String description;
  final double price;
  final String imageUrl;
  bool isFavorite;

  Product({
    required this.id,
    required this.title,
    required this.description,
    required this.price,
    required this.imageUrl,
    this.isFavorite = false,
  });

  void _setFavorite(bool newValue) {
    isFavorite = newValue;
    notifyListeners();
  }

  Map<String, String> createHeaders(String authToken) {
    final String basicAuth =
        'Basic ' + base64Encode(utf8.encode('$authToken:'));

    return {
      HttpHeaders.authorizationHeader: basicAuth,
      HttpHeaders.contentTypeHeader: "application/json",
    };
  }

  Future<void> toggleFavoriteStatus(String authToken, String userId) async {
    final bool oldStatus = isFavorite;
    isFavorite = !isFavorite;
    notifyListeners();

    try {
      final String host = Server.host;
      final Uri url = Uri.parse(host + 'update_favorite');
      final Map data = {
        "productId": id,
        "userId": userId.toString(),
        "isFavorite": isFavorite.toString(),
      };

      final response = await http.post(
        url,
        body: json.encode(data),
        headers: createHeaders(authToken),
      );
      if (response.statusCode >= 400) {
        _setFavorite(oldStatus);
      }
    } catch (error) {
      print(error);
      _setFavorite(oldStatus);
      throw error;
    }
  }
}
